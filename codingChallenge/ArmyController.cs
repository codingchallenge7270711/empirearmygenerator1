using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace EmpireArmyGenerator.Controllers
{
    /// <summary>
    /// Represents the controller for managing armies.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ArmyController : ControllerBase
    {
        private readonly IArmyGenerator _armyGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArmyController"/> class.
        /// </summary>
        /// <param name="armyGenerator">The army generator.</param>
        public ArmyController(IArmyGenerator armyGenerator)
        {
            _armyGenerator = armyGenerator;
        }

        /// <summary>
        /// Generates an army of the specified size.
        /// </summary>
        /// <param name="size">The size of the army.</param>
        /// <returns>The generated army.</returns>
        [HttpPost]
        public ActionResult<Army> GenerateArmy(int size)
        {
            if (size <= 3)
            {
                return BadRequest("Army size must be greater than 0.");
            }

            var army = _armyGenerator.GenerateArmy(size);
            return Ok(army);
        }
    }

    /// <summary>
    /// Represents an army generator.
    /// </summary>
    public interface IArmyGenerator
    {
        /// <summary>
        /// Generates an army of the specified size.
        /// </summary>
        /// <param name="size">The size of the army.</param>
        /// <returns>The generated army.</returns>
        Army GenerateArmy(int size);
    }

    /// <summary>
    /// Represents an army generator.
    /// </summary>
    public class ArmyGenerator : IArmyGenerator
    {
        private static readonly string[] TroopTypes = { "Spearmen", "Swordsmen", "Archers" };
        private readonly Random _random = new Random();

        /// <summary>
        /// Generates an army of the specified size.
        /// </summary>
        /// <param name="size">The size of the army.</param>
        /// <returns>The generated army.</returns>
        public Army GenerateArmy(int size)
        {
            var army = new Army();
            var remainingSize = size;
            var index = 0;

            foreach (var troopType in TroopTypes)
            {
                index++;
                if (remainingSize == 1 || troopType == TroopTypes.Last())
                {
                    army.Troops.Add(new Troop { Type = troopType, Count = remainingSize });
                    remainingSize = 0;
                }
                else
                {
                    var count = _random.Next(1, remainingSize-TroopTypes.Length-index);
                    army.Troops.Add(new Troop { Type = troopType, Count = count });
                    remainingSize -= count;
                }
            }

            return army;
        }
    }

    /// <summary>
    /// Represents an army.
    /// </summary>
    public class Army
    {
        /// <summary>
        /// Represents a list of troops in an army.
        /// </summary>
        public List<Troop> Troops { get; set; } = new List<Troop>();
    }

    /// <summary>
    /// Represents a troop in the army.
    /// </summary>
    public class Troop
    {
        /// <summary>
        /// Gets or sets the type of the troop.
        /// </summary>
        public string? Type { get; set; }

        /// <summary>
        /// Gets or sets the count of the troop.
        /// </summary>
        public int Count { get; set; }
    }
}