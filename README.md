# Empire Army Generator

The Empire Army Generator is a .NET API designed to generate random armies for the Empire game. It enables players to request a random army of a specified size, returning a composition of troops (e.g., Spearmen, Swordsmen, Archers) that constitute the army.

## Features

- Random Army Generation: Generate random armies with a specified size.
- Troop Count Guarantee: Ensure each troop type has a count greater than 0.
- Varied Army Compositions: Provide different army compositions with each request.
- API Documentation: Expose API endpoints using Swagger for easy testing and documentation.

## Prerequisites

- .NET 7.0 SDK
- Visual Studio, Visual Studio Code, or any other preferred IDE

## Installation

Clone work repository to test it on localhost:

```bash
git clone https://gitlab.com/codingchallenge7270711/empirearmygenerator1.git
```

## Usage
when open the project to run the project:
```bash
cd .\codingChallenge\
dotnet run
```
open the link: [http://localhost:5072/swagger/index.html](http://localhost:5072/swagger/index.html)

or in terminal you'll see an open port by your terminal and add to link: `/swagger/index.html`

![screenshot](terminal-screenshoot.jpg)


to run tests: 
```bash
cd .\codingChallenge.Tests\
dotnet test

```
