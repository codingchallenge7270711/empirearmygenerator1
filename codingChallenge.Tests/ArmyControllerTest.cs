using EmpireArmyGenerator.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace codingChallenge.Tests
{
    public class ArmyControllerTests
    {
        [Fact]
        public void GenerateArmy_ValidSize_ReturnsOkResult()
        {
            // Arrange
            var armyGenerator = new ArmyGenerator();
            var controller = new ArmyController(armyGenerator);
            int size = 100;

            // Act
            var result = controller.GenerateArmy(size);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
        }

         [Fact]
        public void GenerateArmy_InvalidSize_ReturnsBadRequest()
        {
            // Arrange
            var armyGenerator = new ArmyGenerator();
            var controller = new ArmyController(armyGenerator);
            int size = -10;

            // Act
            var result = controller.GenerateArmy(size);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public void GenerateArmy_ValidSize_ReturnsArmyWithCorrectSize()
        {
            // Arrange
            var armyGenerator = new ArmyGenerator();
            var controller = new ArmyController(armyGenerator);
            int size = 500;

            // Act
            var result = controller.GenerateArmy(size);
            var okResult = result.Result as OkObjectResult;
            var army = okResult?.Value as Army;

            // Assert
            Assert.NotNull(army);
            Assert.Equal(size, army.Troops.Sum(t => t.Count));
        }

        [Fact]
        public void GenerateArmy_ValidSize_ReturnsArmyWithNonEmptyTroops()
        {
            // Arrange
            var armyGenerator = new ArmyGenerator();
            var controller = new ArmyController(armyGenerator);
            int size = 200;

            // Act
            var result = controller.GenerateArmy(size);
            var okResult = result.Result as OkObjectResult;
            var army = okResult?.Value as Army;

            // Assert
            Assert.NotNull(army);
            Assert.All(army.Troops, troop => Assert.True(troop.Count > 0));
        }
    }
}